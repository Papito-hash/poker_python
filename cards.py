import random


class Card:
    def __init__(self, name, value, suit, symbol):
        self.value = value
        self.suit = suit
        self.name = name
        self.symbol = symbol
        self.showing = False

    def __repr__(self):
        if self.showing:
            return self.symbol
        else:
            return 'Card'


class Deck:
        def shuffle(self, times=1):
            random.shuffle(self.cards)
            print('Deck Shuffled')

        def deal(self):
            return self.cards.pop(0)


class StandardDeck(Deck):
    def __init__(self):
        self.cards = []
        suits = {'hearts':'♡', 'spades':'♠', 'diamonds':'◆', 'clubs':'♣'}
        values = {'two': 2,
                  'three': 3,
                  'four': 4,
                  'five': 5,
                  'six': 6,
                  'seven': 7,
                  'eight': 8,
                  'nine': 9,
                  'ten': 10,
                  'Jack': 11,
                  'Queen': 12,
                  'King': 13,
                  'Ace': 14}

        for name in values:
            for suit in suits:
                symbol_icon = suits[suit]
                if values[name] < 11:
                    symbol = str(values[name])+symbol_icon
                else:
                    symbol = name[0] + symbol_icon
                self.cards.append(Card(name, values[name], suit, symbol))

    def __repr__(self):
        return f'Standard deck of cards: {len(self.cards)} remaining'


class Player:
    def __init__(self):
        self.cards = []

    def card_count(self):
        return len(self.cards)

    def add_card(self, card):
        self.cards.append(card)


class PokerScore:
    def __init__(self, cards):
        if not len(cards) == 5:
            return 'Error wrong number of cards'

        self.cards = cards


    def flush(self):
        suits = [card.suit for card in self.cards]
        if len(set(suits)) == 1:
            return True
        return False

    def straight(self):
        values = [card.value for card in self.cards]
        values.sort()

        if not len(set(values)) == 5:
            return False

        if values[4] == 14 and values[0] == 2 and values[1] == 3 and values[2] == 4 and values[3] == 5:
            return 5
        else:
            if not values[0]  + 1 == values[1]: return False
            if not values[1]  + 1 == values[2]: return False
            if not values[2]  + 1 == values[3]: return False
            if not values[3]  + 1 == values[4]: return False

        return values[4]

    def high_card_value(self):
        values = [card.value for card in self.cards]
        high_card = None
        for card in self.cards:
            if high_card is None:
                high_card = card
            elif high_card.value < card.value:
                high_card = card

        return high_card

    def highest_count(self):
        count = 0
        values = [card.value for card in self.cards]
        for value in values:
            if values.count(value) > count:
                count = values.count(value)

        return count


    def pairs(self):
        pairs1 = []
        values = [card.value for card in self.cards]
        for value in values:
            if values.count(value) == 2 and value not in pairs1:
                pairs1.append(value)


        return pairs1

    def four_kind(self):
        values = [card.value for card in self.cards]
        for value in values:
            if values.count(value) == 4:
                return True

    def full_house(self):
        two = False
        three = False

        values = [card.value for card in self.cards]
        if values.count(values) == 2:
            two = True
        elif values.count(values) == 3:
            three = True

        if two and three:
            return True
        else:
            return False


def interpreter_video_poker():
    player = Player()

    # initial amount
    points = 100

    # cost per hand
    hand_cost = 5

    end = False
    while not end:
        print(f'You have {points} points')
        print()

        points -= 5

        # hand loop
        deck = StandardDeck()
        deck.shuffle()


        # deal out
        for i in range(5):
            player.add_card(deck.deal())

        # Make the cards visible
        for card in player.cards:
            card.showing = True

        print(player.cards)

        valid_input = False
        while not valid_input:
            print('Which card do you want to discard? (ie. 1, 2, 3)')
            print("*Just hit return to hold all, type exit to quit")
            input_str = input()

            if input_str == "exit":
                end = True
                break

            try:
                input_list = [int(inp.strip()) for inp in input_str.split(',') if inp]

                for inp in input_list:
                    if inp > 6:
                        continue
                    if inp < 1:
                        continue
                for inp in input_list:
                    player.cards[inp - 1] = deck.deal()
                    player.cards[inp-1].showing = True

                valid_input = True

            except:
                print('Input Error: use commas to separate the cards you want to hold')


        print(player.cards)

        # Score
        score = PokerScore(player.cards)
        straight = score.straight()
        flush = score.flush()
        high_count = score.highest_count()
        pairs = score.pairs()

        # Game Score

        # Royal Flush
        if straight and flush and straight == 14:
            print('Royal Flush!!!')
            print('+2000')
            points += 2000

        # Straight Flush
        elif straight and flush:
            print('Straight Flush!')
            print('+250')
            points += 250

        # 4 of kind
        elif score.four_kind():
            print('Four of Kind!')
            print('+125')
            points += 125

        # Full House
        elif score.full_house():
            print('Full House!')
            print('+40')
            points += 40


        # Flush
        elif score.flush():
            print('Flush!')
            print('+25')
            points += 25

        # straight
        elif score.straight():
            print('Straight!')
            print('+20')
            points += 20

        # 3 of kind
        elif high_count == 3:
            print('Three of a kind!')
            print('+15')
            points += 15


        # 2 pair
        elif len(pairs) == 2:
            print("Two Pairs!")
            print("+10")
            points += 10


        # jacks or better
        elif pairs and pairs[0] > 10:
            print('Jacks or Better')
            print('+5')
            points += 5


        player.cards = []


        print()
        print()
        print()
